class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :access_token
      t.string :refresh_token
      t.datetime :expires_at
      t.string :code
      t.datetime :last_update

      t.timestamps null: false
    end

    add_index :users, :email
  end
end
