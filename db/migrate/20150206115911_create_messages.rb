class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :user_id
      t.string :message_id
      t.string :message_to
      t.string :subject
      t.text   :body
      t.datetime :send_date

      t.timestamps null: false
    end

    # add_index :messages, [:message_to, :body, :subject], name: 'subject_to_body', type: :fulltext
    # add_index :messages, :body, name: 'subject_to_body', type: :fulltext

    add_index :messages, :message_id
    add_index :messages, :message_to
    add_index :messages, :subject

  end
end
