Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV["GOOGLE_CLIENT_ID"], ENV["GOOGLE_CLIENT_SECRET"], {
	  scope: "email, profile, https://mail.google.com/",
	  access_type: 'offline',
	  approval_prompt: true,
	  select_account: true
	}
end
