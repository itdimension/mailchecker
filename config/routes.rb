Rails.application.routes.draw do

  root to: 'users#index'

  resources :users, only: :index
    get 'users/logout', to: 'users#logout', as: 'logout'
    get 'users/check_messages_count', to: 'users#check_messages_count', as: 'check_messages_count'

  get 'messages/update_message', to: 'messages#update_message', as: 'update_message'
  get 'messages/download_messages', to: 'messages#download_messages', as: 'download_messages'  
  post 'messages/show_message', to: 'messages#show_message', as: 'show_message'
  match 'messages/find_messages', to: 'messages#find_messages', as: 'find_messages', via: [:get, :post]
  
  get "/auth/:provider/callback" => 'sessions#create'
  get "/auth/failure" => 'sessions#error'

end
