class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user

  def current_user
  	current_user ||= User.find_by_email(session[:user_email]) if session[:user_email]
  end

  def current_messages
  	current_messages = current_user.messages.page(params[:page]).per(10).order('id DESC')
  end


end
