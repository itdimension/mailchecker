class UsersController < ApplicationController

	def index
  	@user = current_user
    if @user
 		  @messages = current_messages      
    end
	end

	def logout
		session[:user_email] = nil
		session[:gmail_access_token] = nil
		redirect_to root_path
	end

	def check_messages_count
		count = current_user.messages.count
		render json: {count: "#{count}"}
	end

end
