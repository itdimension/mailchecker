class SessionsController < ApplicationController
  layout false

  def index
  end

  def create
    auth = request.env["omniauth.auth"]
    session[:gmail_access_token] = auth[:credentials][:token]
    session[:user_email] = auth[:extra][:raw_info][:email]
    user = User.find_or_initialize_by(email: auth[:extra][:raw_info][:email])

    user.access_token = auth[:credentials][:token]
    user.refresh_token = auth[:credentials][:refresh_token] if auth[:credentials][:refresh_token].present?
    user.expires_at = Time.at(auth[:credentials][:expires_at]).to_datetime
    user.code = params[:code]
    user.save

    count = user.show_message_count  	

    redirect_to controller: 'users', action: 'index', mcount: "#{count}"
  end  

  def error
    render html: "<h3 style='margin-left:35%;'>Authentication failure. Please try again
     <a href=#{root_path}>Main page</a></h3>".html_safe
  end

end
