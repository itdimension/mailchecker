class MessagesController < ApplicationController

	def download_messages
		Thread.new do
			current_user.download_messages
		end
		respond_to do |format|
			format.js 
			format.html {redirect_to root_path}	
		end	
	end

	def update_message
		user = current_user
		user.download_messages

		@messages = current_messages

		respond_to do |format|
			format.js 
			format.html {redirect_to root_path}	
		end		
	end

	def show_message
		user = current_user
		message = user.messages.find_by_message_id(params[:message_name])
		if message
			render json: {
				to: "#{message.message_to}",
				subject: "#{message.subject}",
				message: "#{message.body}"
			}
		else
			render json: {data: 'Not found'}
		end
	end

	def find_messages
		@messages = Message.letters_search(params[:message][:data], current_user.id)
		.page(params[:page]).per(10).order('id DESC')

		respond_to do |format|
			format.js
		end
	end

end
