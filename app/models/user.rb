class User < ActiveRecord::Base
	has_many :messages

	def read_user_info
		@email = self.email
		@token = self.access_token
	end

	def imap_connection
    @imap = Net::IMAP.new('imap.gmail.com', 993, usessl = true, certs = nil, verify = false)
    begin
    	@imap.authenticate('XOAUTH2', @email, @token)
    rescue => error
    	@imap.disconnect()
			logger.debug "ERROR - #{error}"
			return false
		else
			@imap.list('*', '*').each do |box|
				@mailbox_name = box.name if box.attr.include?(:Sent)
			end
			@imap.select("#{@mailbox_name}")			
			@imap
		end
	end

	def download_messages(last_update = self.last_update)
		token_expires?
		read_user_info

		if last_update
			today = Net::IMAP.format_date(last_update)
			search_params = ['SINCE', today]
		else
			search_params = ['ALL']
		end
		
		imap = imap_connection
		if imap

			imap.search(search_params).each do |message_id|				
			  msg = imap.fetch(message_id, 'RFC822')[0].attr['RFC822']
				email = Mail.new(msg)
					
				message_already_added?(email) if last_update
				email_processing(email) if !last_update			
			end

			self.update(last_update: DateTime.now.to_date)	
			imap.logout()
			imap.disconnect()	
		end		
	end

	def show_message_count
		read_user_info
		imap = imap_connection
		if imap
	    messages_count = imap.status("#{@mailbox_name}", ['MESSAGES'])['MESSAGES']
			imap.logout()
			imap.disconnect()	
			messages_count
		end
	end

	def message_already_added?(email)
		id = email.message_id
		message = Message.find_by_message_id(id)
		email_processing(email) if message.nil?
	end

	def email_processing(email)
		if email.multipart?
	    charset = email.text_part.content_type_parameters[:charset]
	    email_body = email.text_part.body.to_s.force_encoding(charset).encode("UTF-8")
		else
	    charset = email.content_type_parameters[:charset]
	    email_body = email.body.decoded.force_encoding(charset).encode("UTF-8")
		end

		self.messages.create(
			message_id: email.message_id,
			message_to: email.to.join(', '),
			subject: email.subject,
			body: email_body,
			send_date: email.date.to_s			
		)
	end
 
  def request_token_from_google
		params = {'refresh_token' => self.refresh_token,
				    	'client_id' => ENV["GOOGLE_CLIENT_ID"],
				    	'client_secret' => ENV["GOOGLE_CLIENT_SECRET"],
				    	'grant_type' => 'refresh_token'
				  	 }
    url = URI("https://accounts.google.com/o/oauth2/token")
    Net::HTTP.post_form(url, params)
  end
 
  def refresh!
    response = request_token_from_google
    data = JSON.parse(response.body)
    update_attributes(
    access_token: data['access_token'],
    expires_at: Time.now + (data['expires_in'].to_i).seconds)
  end
 
  def token_expires?
    if expires_at < Time.now
    	refresh!
    end
  end

end
