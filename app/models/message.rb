class Message < ActiveRecord::Base
	belongs_to :user

	validates_uniqueness_of :message_id

	def self.letters_search(search_params, user_id)
		u_mes = user_messages(user_id)
		result = u_mes.where('body like ? OR message_to like ? OR subject like ?',
		 "%#{search_params}%", "%#{search_params}%", "%#{search_params}%")
		result
	end

	scope :user_messages, ->(id) {where(user_id: id)}
end
