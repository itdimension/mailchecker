module ApplicationHelper

	def subjects(subject)
		if subject.present?
			return subject
		else
			return 'No subject'
		end
	end

# flash messages

  def flash_class(flash_type)
    case flash_type
      when 'success'
        "alert-success" # Green
      when 'error'
        "alert-danger" # Red
      when 'alert'
        "alert-warning" # Yellow
      when 'notice'
        "alert-info" # Blue
      else
        flash_type.to_s
    end
  end

end
