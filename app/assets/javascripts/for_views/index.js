$(document).ready(function() {
	$('.table_container td b, .table_container td p, .table_container td br')
	.addClass('linear_body_');
	$('#message_data').val('');
});

var message_name = '';
$('table').bind('click', '.message_info_view', function(e) {
	message_name = $(e.target).parents(".message_info_view").attr('data-name');

	if (message_name) {
		$('#message').css({'display':'block','pointer-events':'auto'});
		load_message(e, message_name);
	};	
});

function load_message(e, massage_name) {
	var request = $.ajax({
		url: '/messages/show_message',
		type:'POST',
		data: {message_name: massage_name},
		success: function(data){
			$('#mf_subject span:last').text(data.subject);
			$('#mf_to span:last').text(data.to);
			$('#mf_body').html(data.message);
		}
	});
};

function message_close() {
	$('#message').hide();
	$('#mf_subject span:last').text('');
	$('#mf_to span:last').text('');
	$('#mf_body').html('');
};

$('#refresh_btn').bind('click', function(){
	$(this).attr('disabled','disabled');
	$('.pagination').hide();
	$('.progress_block').css('display', 'block');
});

var message_count = $('.messages_count').text();
var total_count = parseInt(window.location.href.split('?mcount=')[1]);
var progress_item = 100 / total_count;
var check_count = '';
if (message_count && message_count == 0 && total_count) {

	$('#refresh_btn').attr('disabled','disabled');
	$('.pagination').hide();
	$('.progress_block').css('display', 'block');
	$.ajax({url: '/messages/download_messages'});

	check_count = setInterval(check_count_func, 3000);
	$('.progress .progress-bar').css('width', progress_item+'%');
};

function check_count_func() {
	var resp = $.ajax({
		url: '/users/check_messages_count',
		success: function(data) {
			var load_progress = parseInt(progress_item*data.count);
			if(load_progress > 100){load_progress = 100;};
			if(load_progress < 1){load_progress = 1;};
			$('.progress .progress-bar').css('width', load_progress+'%');

			if(data.count == total_count){
				clearInterval(check_count);
				check_count = '';
				$('#refresh_btn').trigger('click');
				flash_message('Messages successfully loaded');
			};
		}
	});
};

window.onbeforeunload = function() {
	if(check_count || $('.progress_block').is(":visible")) {
		return "Are you want to interrupt downloading?";
	}  
};

function flash_message(message) {
	$('<div/>').prependTo('body')
	.addClass('alert alert-dismissable alert-info fade in')
	.text(message)
	.append('<button class="close" aria-hidden="true" data-dismiss="alert"\
	 type="button">×</button>');

	setTimeout(function(){
		$('.alert').remove();
	}, 2000)
};
